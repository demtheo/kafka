const { Kafka } = require('kafkajs')

const kafka = new Kafka({
  clientId: 'my-app',
  brokers: ['localhost:9092']
})

async function send(){
  const producer = kafka.producer()
  await producer.connect()
  await producer.send({
    topic: 'test_topic',
    messages: [
      { value: 'Hello KafkaJS user!' },
    ],
  })
  
  await producer.disconnect()
}

//send()

const handlers = {
  group1: ()=>{
    //throw new Error('Somethisg whent wrong')
    console.log('group1 succeed')
  },
  group2: ()=>{
    //throw new Error('Somethisg whent wrong')
    console.log('group2 succeed')
  },
  group3: ()=>{
    //throw new Error('Somethisg whent wrong')
    console.log('group3 succeed')
  }
}

const groups = ['group1', 'group2', 'group3']

groups.map(async(g)=> {
  const consumer = kafka.consumer({ groupId: g })
  const handler = handlers[g]
  await consumer.connect()
  await consumer.subscribe({ topic: 'test_topic', fromBeginning: false })

  consumer.run({
    autoCommit: false,
    eachBatch: async ({
        batch,
        resolveOffset,
        heartbeat,
        commitOffsetsIfNecessary,
        uncommittedOffsets,
        isRunning,
        isStale,
    }) => {
        let toCommit = []
        for (let message of batch.messages) {
            if (!isRunning()) break
            console.log(g, message.offset)
            try{
              handler()
              console.log('hit', g)
              toCommit.push({topic: batch.topic, offset: message.offset, partition: batch.partition})
            } catch {
              consumer.pause([{topic: batch.topic, partitions:[batch.partition]}])
            }
        }
        console.log('toCommit', toCommit)
        await consumer.commitOffsets(toCommit).then(()=> {
          toCommit = []
        })
        await heartbeat()
    },
  })
  
})



